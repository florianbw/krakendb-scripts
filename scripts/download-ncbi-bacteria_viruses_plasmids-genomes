#!/bin/bash

set -eu -o pipefail

USAGE="USAGE: `basename $0` [OPTIONS]

OPTIONS:
  -b  Download bacteria genomes
  -v  Download virus genomes
  -p  Download plasmid sequences
"

ORGANISMS=""

while getopts ":bvp" opt; do
  case "${opt}" in
    b)
      echo "-b was triggered, downloading bacteria"
	  ORGANISMS="$ORGANISMS Bacteria"
      ;;
    v)
      echo "-v was triggered, downloading viruses"
	  ORGANISMS="$ORGANISMS Viruses"
      ;;
    p)
      echo "-p was triggered, downloading plasmids"
	  ORGANISMS="$ORGANISMS Plasmids"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

[[ "$ORGANISMS" != "" ]] || ( echo "Set one or more of -b -v and -p" && echo "$USAGE" && exit 1 )

DIR="${1:-_download}"
DUSTED_DIR="${2:-library}"
[[ -d $DIR ]] || mkdir -vp $DIR
[[ -d $DUSTED_DIR ]] || mkdir -vp $DUSTED_DIR

SCRIPT_DIR=`dirname $0`
RSYNC_ARGS=(--partial -a -i '--include=*.fna' --no-motd --delete '--exclude=*.*' --delete-excluded)

for ORGANISM in $ORGANISMS; do
	echo "Syncing $ORGANISM genomes to $DIR ..."
    rsync "${RSYNC_ARGS[@]}" "ftp.ncbi.nih.gov::genomes/$ORGANISM" $DIR | tee -a $DIR/$ORGANISM.log
    DIR=$DIR DUSTED_DIR=$DUSTED_DIR $SCRIPT_DIR/process-rsync-log $ORGANISM
done

