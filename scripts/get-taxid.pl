#! /usr/bin/env perl
#
# Short description for get-taxid.pl
#
# Author fbreitwieser <fbreitwieser@igm3>
# Version 0.1
# Copyright (C) 2015 fbreitwieser <fbreitwieser@igm3>
# Modified On 2015-03-16 16:30
# Created  2015-03-16 16:30
#
use strict;
use warnings;

use Bio::DB::EUtilities;
my $factory = Bio::DB::EUtilities->new(-eutil => 'esummary',
                                       -email => 'mymail@foo.bar',
                                       -db    => 'taxonomy',
                                       -term  => $ARGV[0] );

my ($id)  = $factory->next_DocSum->get_contents_by_name('Id');
 
print "$id\n";
 
