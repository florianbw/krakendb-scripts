# Kraken database scripts #

This repository provides scripts to download and preprocess genomes from various sources, and build a Kraken database. The scripts can be called through make targets.


## Requirements ##

 - rsync
 - jellyfish 1
 - kraken


## Usage ##

The default target of the Makefile calls the targets 'taxonomy', 'contaminants', 'bacteria', 'viruses', 'plasmids', and 'build-db', which download the taxonomy, contaminant sequences, bacterial and viral genomes and plasmids, and build the database:


```
#!sh

make DL=/path/to/downloaddir DB=/path/to/krakendb all
```


The targets 'human', 'fungi', and 'protists' can be used to download the human genome, and specified fungal and protist genomes that are associated with human diseases. For a full build including human, fungal and protist genomes, call


```
#!sh

make DL=/path/to/downloaddir DB=/path/to/krakendb human fungi protists all
```

## Fungal and protist genomes ##
Currently included fungal genomes:

- Aspergillus fumigatus
- Candida albicans
- Candida dubliniensis
- Candida glabrata
- Cryptococcus gattii
- Cryptococcus neoformans
- Encephalitozoon cuniculi
- Encephalitozoon intestinalis

Currently included protist genomes:

- Plasmodium falciparum
- Leishmania major
- Toxoplasma gondii
- Trypanosoma brucei
- Trypanosoma cruzi